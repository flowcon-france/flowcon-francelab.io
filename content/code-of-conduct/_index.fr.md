+++
+++

Tous les participants, intervenants, sponsors et bénévoles de notre conférence sont tenus d'accepter le code de conduite suivant. Les organisateurs feront respecter ce code tout au long de l'événement. Nous attendons la coopération de tous les participants afin de garantir un environnement sûr pour tous.
 
# Need Help? 

Vous avez nos coordonnées dans les e-mails que nous avons envoyés.
 
# La version rapide

Notre conférence se veut une expérience sans harcèlement, quel que soit votre sexe, votre identité sexuelle, votre âge, votre orientation sexuelle, votre handicap, votre apparence physique, votre poids, votre groupe ethnique ou votre religion. Nous ne tolérons aucun harcèlement des participant‧e‧s à la conférence, sous quelque forme que ce soit.
Les expressions et les images à connotation sexuelle ne sont pas appropriées lors de l'événement. Ceci inclut les conférences, les ateliers, les soirées, Twitter et les autres médias en ligne. L'organisation s'engage à prendre des sanctions appropriées pour les personnes qui violent ces règles, jusqu'à l'exclusion __sans remboursement__ et le recours légal.
 
# La version moins rapide

Le harcèlement inclut des commentaires à l'oral ou à l'écrit sur le sexe, l'identité sexuelle, l'âge, l'orientation sexuelle, le handicap, l'apparence physique, le poids, le groupe ethnique, la religion, les images à connotation sexuelle dans des lieux publics, les intimidations délibérées, la traque, la poursuite, un harcèlement photographique ou vidéo, une suite d'interruptions des conférences et des autres événements, un contact physique inapproprié et des avances sexuelles non désirées.
 
Les participant‧e‧s à qui il sera demandé d'arrêter tout comportement de harcèlement doivent arrêter immédiatement.

Les sponsors sont aussi sujet à la politique anti-harcèlement. En particulier, les sponsors ne doivent pas utiliser d'images ou de supports à connotation sexuelle. Ils ne doivent pas non plus se livrer à des activités à connotation sexuelle. L'équipe du stand (y compris les bénévoles) ne doit pas utiliser de vêtements, uniformes ou costumes à connotation sexuelle. Ils ne doivent pas non plus créer un environnement sexualisé.

Face à un comportement de harcèlement, l'équipe d'organisation de la conférence peut prendre toute action qui lui semble adéquate. Cela va d'un simple avertissement à l'exclusion sans remboursement de la conférence.

Si vous vous sentez harcelé‧e, si vous pensez que quelqu'un se fait harceler, et plus généralement en cas de problème, merci de contacter immédiatement l’équipe d’organisation de l'événement. L'organisation s'assure que ses membres sont facilement identifiables (badge, brassard, tour de cou, t-shirt…) présentés au début de la conférence.

Les membres de l'organisation seront ravi‧e‧s d'aider les participants à contacter la sécurité de l'hôtel ou du bâtiment où se déroule l'événement, ou les forces de l'ordre ; à fournir une escorte ainsi qu'à aider de toute autre façon les personnes victimes de harcèlement, pour garantir leur sécurité pendant la durée de l'événement. Nous apprécions votre participation à l'événement.

Nous attendons de chacun‧e le respect de ces règles dans le bâtiment des conférences et des ateliers, ainsi que pendant les événements sociaux relatifs à la conférence.
